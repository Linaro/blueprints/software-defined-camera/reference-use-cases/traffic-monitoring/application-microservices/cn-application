"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
from src.utils import get_final_class_count
from src.img_helper import _draw_bounding_boxes_tiny, postprocess_img
import numpy as np


def postprocess_results(right_boxes, right_classes, right_scores, frame, img, names, model):
    """
    Postprocesses object detection results and visualizes them on the input image.

    This function takes the detected bounding boxes, corresponding classes, scores,
    the original frame, input image, class names, and the model as input. It processes
    the results, draws bounding boxes on the image, and returns class count statistics.

    Args:
        right_boxes (list): List of detected bounding boxes.
        right_classes (list): List of corresponding class labels for the detections.
        right_scores (list): List of confidence scores for the detections.
        frame (numpy.ndarray): Original image frame.
        img (numpy.ndarray): Input image.
        names (list): List of class names.
        model: The object detection model used.

    Returns:
        tuple: A tuple containing an array of class count statistics and the output image.

    """
    if right_boxes == 0:
        return [0, 0, 0, 0, 0], frame
    else:
        right_boxes = np.array(right_boxes)
        right_classes = np.array(right_classes)
        right_scores = np.array(right_scores)
        img, right_boxes = postprocess_img(
            img*255, frame.shape[1::-1], right_boxes)
        arr = get_final_class_count(right_classes, model)
        if right_boxes is not None:
            img = _draw_bounding_boxes_tiny(
                frame, right_boxes, right_scores, right_classes, names, img.shape[:2])

        return arr, img
