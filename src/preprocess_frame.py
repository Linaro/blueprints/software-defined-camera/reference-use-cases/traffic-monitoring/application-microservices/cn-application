"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
import cv2
from src.img_helper import preprocess_img

# List of valid object detection models
VALID_MODELS = ["tiny_yolov3", "yolov3"]


def preprocess_frame(frame, model):
    """
    Preprocess a video frame for object detection using a specified model.

    This function takes a video frame and preprocesses it for object detection using
    the given model. The frame is resized to a standard size, and if the model is either
    "tiny_yolov3" or "yolov3", color conversion and additional preprocessing are applied.

    Args:
        frame (numpy.ndarray): The input video frame as a NumPy array.
        model (str): The name of the object detection model to be used.

    Returns:
        numpy.ndarray: The preprocessed image ready for object detection.

    """
    if model in VALID_MODELS:
        img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        img = preprocess_img(img, (416, 416))
        img = img[0]
    else:
        img = cv2.resize(frame, (416, 416))

    return img
