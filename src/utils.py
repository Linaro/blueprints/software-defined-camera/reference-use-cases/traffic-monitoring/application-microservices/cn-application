"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""

# List of valid object detection models
VALID_MODELS = ["tiny_yolov3", "yolov3"]


def get_final_class_count(classes, model):
    """
    Count the occurrences of each class in the given list based on the specified model.

    This function takes a list of class labels and a model name, and returns an array
    containing the count of occurrences of each class. If the model name is in the list
    of valid models, the counts are calculated; otherwise, the array will contain zeros.

    Args:
        classes (list): List of class labels.
        model (str): The name of the object detection model.

    Returns:
        list: An array containing the count of occurrences for each class.

    """
    arr = [0] * 5
    for i in list(classes):
        if model in VALID_MODELS:
            arr[i] = arr[i] + 1
    return arr
